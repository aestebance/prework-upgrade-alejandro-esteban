function calculoDNI(dni, letra){
    const letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 
                            'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

    if (dni >= 0 && dni <= 99999999) {
        const modLetter = dni%23;
        if (letra == letras[modLetter]) {
            console.log("DNI válido");
        } else {
            console.log("DNI incorrecto");
        }
    }
}


let readline = require('readline-sync');
let dni = readline.question("What is your DNI?:");
let letra = readline.question("What is the letter?:");

calculoDNI(dni, letra);