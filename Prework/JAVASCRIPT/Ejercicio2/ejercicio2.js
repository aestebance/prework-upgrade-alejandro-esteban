function clasificarRuedas(diametro) {
    if (diametro <= 10) {
        console.log("Es una rueda para un juguete pequeño");
    }
    else if (diametro < 20) {
        console.log("Es una rueda para un jueguete mediano");
    } else {
        console.log("Es una rueda para un juguete grande");
    }
}

clasificarRuedas(5);
clasificarRuedas(7);
clasificarRuedas(14);
clasificarRuedas(31);