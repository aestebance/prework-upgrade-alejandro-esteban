<?php 
    function diaSemana($dia) {
        switch ($dia) {
            case 1:
                echo "Lunes";
                break;
            case 2:
                echo "Martes";
                break;
            case 3:
                echo "Miércoles";
                break;
            case 4:
                echo "Jueves";
                break;
            case 5:
                echo "Viernes";
                break;
            case 6:
                echo "Sábado";
                break;
            case 7:
                echo "Domingo";
                break;
            default:
                echo "Introduce un número entre el 1 y el 7";
        }
    }

    diaSemana(1);
    diaSemana(2);
    diaSemana(3);
?>