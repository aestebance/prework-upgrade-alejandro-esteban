<?php
    function compruebaPalabra($direccion) {
        $descriptor = fopen($direccion, 'r');
        $i = 0;
        while(($contenido = fgets($descriptor)) != false) {
           $i = $i + substr_count(strtoupper($contenido), 'MOLINO');
        }
        fclose($descriptor);
        echo "La palabra molino aparece " . $i . " veces.";
    }

    compruebaPalabra("https://gist.githubusercontent.com/jsdario/6d6c69398cb0c73111e49f1218960f79/raw/8d4fc4548d437e2a7203a5aeeace5477f598827d/el_quijote.txt");
?>