<?php
    function vocales($frase) {
        $vocales = [false, false, false, false, false];
        for ($i=0; $i<strlen($frase); $i++) {
            switch($frase[$i]) {
                case 'a':
                    $vocales[0] = true;
                    break;
                case 'e':
                    $vocales[1] = true;
                    break;
                case 'i':
                    $vocales[2] = true;
                    break;
                case 'o':
                    $vocales[3] = true;
                    break;
                case 'u':
                    $vocales[4] = true;
                    break;
                default:
                    break;
            }
        }

        if (in_array(false, $vocales))
        {
            return false;
        }
        else {
            return true;
        }
    }


    if (isset($_POST['frase'])) {
        $frase = $_POST['frase'];

        if(vocales($frase)) {
            echo "LA PALABRA CONTIENE LAS 5 VOCALES";
        }
        else {
            echo "NO CONTIENE TODAS LAS VOCALES";
        }
    }
    ?>
    <html>
    <head><title>Ejercicio2</title></head>

    <body>
    <form action="index.php" method="POST"> 
        <input name="frase" type="text" /> 
        <input type="submit" value="Comprobar" />
    </form>
    </body>
    </html>