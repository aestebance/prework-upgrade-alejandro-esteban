<?php
    function copiar() {
        $archivos = scandir('.\\');

        /*
        No es posible asignar ":" como nombre de un fichero/directorio en windows. Se sustituye por ";". 
        
        Se copian todos los archivos, incluído el propio php
        */

        $fecha = getdate();
        $fecha = $fecha["year"] . "-" . $fecha["mon"] . "-" . $fecha["mday"] . " " . $fecha["hours"] . ";" . $fecha["minutes"];
        mkdir($fecha);

        foreach ($archivos as $valor) {
            if ($valor != "." && $valor != "..") {
                $ficheroLeer = fopen($valor, 'r');
                $ficheroEscribir = fopen($fecha . "\\" . $valor . ".modificado", 'w+');
                while (($contenido = fgets($ficheroLeer)) != false) {
                    fwrite($ficheroEscribir, $contenido);
                }
            }
        }
    }
    copiar();
?>
