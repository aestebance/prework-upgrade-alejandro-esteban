<?php
    function reemplazar($dirLeer, $dirEscribir, $buscar, $reemplazar) {
        $libroLeer = fopen($dirLeer, 'r');
        $libroEscribir = fopen($dirEscribir, 'w+');

        while(($contenido = fgets($libroLeer)) != false) {
           fwrite($libroEscribir, str_replace($buscar, $reemplazar, $contenido));
        }
        fclose($libroEscribir);
        fclose($libroLeer);
    }

    reemplazar("https://gist.githubusercontent.com/jsdario/6d6c69398cb0c73111e49f1218960f79/raw/8d4fc4548d437e2a7203a5aeeace5477f598827d/el_quijote.txt","./quijote.txt", "Sancho", "Morty");
?>